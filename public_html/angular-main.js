var appMaps = angular.module('appMaps', ['uiGmapgoogle-maps']);

appMaps.filter("sanitize", ['$sce', function($sce) {
	return function(htmlCode){
		return $sce.trustAsHtml(htmlCode);
	}
}]);

appMaps.controller('mainCtrl', function($scope, $http) {
	$scope.googleMap = {};
	$scope.map = {center: {latitude: 33.942214, longitude: -83.374536 }, zoom: 14 };
	$scope.options = {rotateControl: true, streetViewControl: false, panControl: true, panControlOptions: {position: "BOTTOM_RIGHT"}};

	$scope.routes = [];
	$scope.vehicles = [];

	$scope.announcement = {
		visible: false
	}
	$scope.topMessage = "There are no messages.";

	//Get routes
	//$http.get('js/routessample.json')
	$http.get('http://www.transit.uga.edu/portal/feed/v3/UGA/masterRoutes/')
	.success(function(data, status, headers, config){
		var count = 0;
		data.data.forEach(function(elem, index, array){
			$scope.routes[count] = elem;
			$scope.routes[count].path = [];
			$scope.routes[count].count = count;
			$scope.routes[count].stroke = {
				color: elem.color
			};
			$scope.routes[count].visible = true;
			count++;
		});

		if(count == 0){
			$scope.announcement.message = "There are no routes availible";
			$scope.announcement.visible = true;
		}

		$scope.routes.forEach(function(elem, index, array){
			$http.get('http://www.transit.uga.edu/portal/feed/v3/UGA/landRoute/'+elem.id)
			.success(function(data, status, headers, config){
				data.data[0].landRoutePoints.forEach(function(elem2, index2, array2){
					$scope.routes[index].path.push(new google.maps.LatLng(elem2.lat, elem2.lon));
					if (index2 === array2.length - 1){
						$scope.loadRouteToMap(index);
					}
				});
			});
		});
		$scope.getStopData();
	});

	//Get vehicles
	$scope.vehicleRawData = [];
	var firstCall = true;
	$scope.getVehicleData = function(){
		$http.get('http://www.transit.uga.edu/portal/feed/v3/UGA/vehicles/')
		.success(function(data, status, headers, config){
			var count = 0;
			data.data.forEach(function(elem, index, array){
				$scope.vehicleRawData[index] = elem;
				if(firstCall) $scope.vehicles[index] = {};
				try{
					if(!("internalVehicleId" in $scope.vehicles[index])){
						$scope.vehicles[index] = elem;
						if($scope.vehicles[index].masterRouteId != null){
							count++;
							$scope.vehicles[index].gMapObject = new google.maps.Marker({
								position: new google.maps.LatLng(elem.latitude, elem.longitude),
								map: $scope.googleMap.getGMap()
							});
							setBusSettings(index);
						}
					}else{
						if($scope.vehicles[index].masterRouteId != null){
							count++;
							$scope.vehicles[index].gMapObject.setPosition(new google.maps.LatLng(elem.latitude, elem.longitude));
							setBusSettings(index);
						}
					}
				}catch(e){}
			});
			firstCall = false;
			if(count == 0){
				$scope.announcement.message = "There are no busses availible.";
				$scope.announcement.visible = true;
			}
		});
	}
	var vehicleRefresh = window.setInterval($scope.getVehicleData, 500);

	function setBusSettings(index){
		if($scope.vehicles[index].masterRouteshortName == null) $scope.vehicles[index].gMapObject.setVisible(false);
		var routeID = arrayObjectIndexOf($scope.routes, $scope.vehicles[index].masterRouteshortName, "shortName");
		if(routeID)	$scope.vehicles[index].gMapObject.setVisible($scope.routes[routeID].gMapObject.visible);
		var symbol = {
			path: google.maps.SymbolPath.CIRCLE,
			scale: 7,
			strokeColor: "#fff",
			strokeWeight: 2,
			fillColor: $scope.routes[routeID].color,
			fillOpacity: 1,
			rotation: 0
		};
		// ARROW ICONS USING TABLET HEADING VECTOR
		/*if($scope.vehicles[index].heading != null){
			symbol.rotation = $scope.vehicles[index].heading;
			symbol.path = google.maps.SymbolPath.FORWARD_CLOSED_ARROW;
		}*/
		$scope.vehicles[index].gMapObject.setIcon(symbol);

	}

	$scope.loadRouteToMap = function(index){
		var polygon = new google.maps.Polygon({
			paths: $scope.routes[index].path,
			strokeColor: $scope.routes[index].stroke.color,
			strokeOpacity: 1,
			strokeWeight: 2,
			fillColor: '#000',
			fillOpacity: 0
		});
		$scope.routes[index].gMapObject = polygon;
		$scope.routes[index].gMapObject.setMap($scope.googleMap.getGMap());
	}

	$scope.toggleRouteVisibility = function(routeid){
		$scope.routes[routeid].gMapObject.setVisible(!$scope.routes[routeid].gMapObject.getVisible());
	}
	$scope.toggleAllRoutesVisibility = function(bool){
		$scope.routes.forEach(function(elem, index, array){
			$scope.routes[index].gMapObject.setVisible(bool);
		});
	}
	function arrayObjectIndexOf(myArray, searchTerm, property) {
		for(var i = 0, len = myArray.length; i < len; i++) {
			if (myArray[i][property] === searchTerm) return i;
		}
		return -1;
	}


	//Get Stop data
	$scope.stopRawData = [];
	$scope.getStopData = function(){
		var count = 0;
		$scope.routes.forEach(function(elem, index, array){
			$http.get('http://www.transit.uga.edu/portal/feed/v3/UGA/stops/'+elem.id)
			.success(function(data, status, headers, config){
				data.data.forEach(function(elem2, index2, array2){
					if(!stopIsInRawData(elem2)){
						$scope.stopRawData.push(elem2);
					}
				});
				count++;
				if(count == $scope.routes.length) $scope.makeStopMarkers();
			});
		});
	}
	function stopIsInRawData(stop){
		var results = jQuery.grep($scope.stopRawData, function(e){ return e.id == stop.id; } );
		if(results.length > 0) return true;
		return false;
	}

	$scope.makeStopMarkers = function(){
		$scope.stopRawData.forEach(function(elem, index, array){
			var busStopIcon = {
				url: 'assets/bus-stop.png',
				size: new google.maps.Size(17, 32),
				origin: new google.maps.Point(0,0),
				anchor: new google.maps.Point(32, 32)
			};
			elem.gMapObject = new google.maps.Marker({
								position: new google.maps.LatLng(elem.latitude, elem.longitude),
								map: $scope.googleMap.getGMap(),
								icon: busStopIcon,
								title: elem.id
							});
			elem.gMapObject.setVisible(false);
			$scope.stopsVisibilityOppositeStatus = "On";
			//elem.gMapObject.setIcon(symbol);
		});
	}
	$scope.toggleStopMarkersVisibility = function(){
		if($scope.stopsVisibilityOppositeStatus == "Off"){
			$scope.stopRawData.forEach(function(elem){
				elem.gMapObject.setVisible(false);
			});
			$scope.stopsVisibilityOppositeStatus = "On";
		}else{
			$scope.stopRawData.forEach(function(elem){
				elem.gMapObject.setVisible(true);
			});
			$scope.stopsVisibilityOppositeStatus = "Off";
		}
	}



	//Geolocation
	var currentLocationIcon = {
		url: 'assets/current_location.gif',
		size: new google.maps.Size(64, 64),
		origin: new google.maps.Point(0,0),
		anchor: new google.maps.Point(32, 32)
	};
	var geoRefresh;
	var initialLocation;
	var currentLocationMarker;
	var mapIsReady = !jQuery.isEmptyObject($scope.googleMap);
	initialLocation = new google.maps.LatLng($scope.map.center.latitude,$scope.map.center.longitude);
	currentLocationMarker = new google.maps.Marker({
		position: initialLocation,
		icon: currentLocationIcon,
		optimized: false
	});
	if(navigator.geolocation) {
		geolocation();
		geoRefresh = window.setInterval(geolocation, 1000);
	}else{
		$scope.geolocation = false;
	}

	var firstRun = true;
	function geolocation(){
		mapIsReady = !jQuery.isEmptyObject($scope.googleMap);
		if(mapIsReady){
			navigator.geolocation.getCurrentPosition(function(position) {
				if(firstRun) $scope.geolocation = true;
				initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
				currentLocationMarker.setPosition(initialLocation);
				if(firstRun) currentLocationMarker.setMap($scope.googleMap.getGMap());
				//if(firstRun) $scope.googleMap.getGMap().setCenter(initialLocation);
				firstRun = false;
			}, function(error){
				console.log(error);
				clearInterval(geoRefresh);
				firstRun = false;
			});
		}
	}



	// Get closest bustop
	var closestStop = {};
	$scope.getClosestBustopID = function(){
		$scope.stopRawData.forEach(function(elem){
			var stop = elem;
			if(stop.latitude < 0) stop.latitude = stop.latitude*(-1);
			if(stop.longitude < 0) stop.longitude = stop.longitude*(-1);

			var userLat = initialLocation.lat();
			var userLng = initialLocation.lng();
			if(userLat < 0) userLat = userLat*(-1);
			if(userLng < 0) userLng = userLng*(-1);

			var latDiff = stop.latitude - userLat;
			var lngDiff = stop.longitude - userLng;
			if(latDiff < 0) latDiff = latDiff*(-1);
			if(lngDiff < 0) lngDiff = lngDiff*(-1);

			var DistanceFromCurrentLocation = Math.sqrt((latDiff*latDiff) + (lngDiff*lngDiff));

			if((typeof(closestStop.distance) === "undefined") || (closestStop.distance > DistanceFromCurrentLocation)){
				closestStop = elem;
				if(closestStop.longitude > 0) closestStop.longitude = closestStop.longitude*-1;
				closestStop.distance = DistanceFromCurrentLocation;
			}
		});
		if(closestStop != {}){
			$scope.topMessage = "<p>The nearest bustop to you is <strong>"+closestStop.stopName+"</strong>.</p>";
			$scope.topMessage += "</br><a onclick=\"zoomToCurrentBusStop()\" class='button center'>Set as current bustop</a><p style='height: 20px'> </p>";
			jQuery(".top-announcement div").click();
			closemenu();
			console.log(closestStop);
			return closestStop;
		}else{
			return 0;
		}
	}
	$scope.zoomToCurrentBusStop = function(){
		console.log(closestStop);
		$scope.googleMap.getGMap().setCenter({lat: closestStop.latitude, lng: closestStop.longitude});
		$scope.googleMap.getGMap().setZoom(17);
		toggleTopAnnouncement();
		if($scope.stopsVisibilityOppositeStatus == "On") $scope.toggleStopMarkersVisibility();

		$scope.infoWindowContent = "";

		$scope.infowindow = new google.maps.InfoWindow({
			content: $scope.infoWindowContent,
			maxWidth: 200
		});
		google.maps.event.addListener(closestStop.gMapObject, 'click', function() {
			$scope.infowindow.open($scope.googleMap.getGMap(), closestStop.gMapObject);
		});
		google.maps.event.trigger(closestStop.gMapObject, 'click');
		$scope.getNextFiveBusses(closestStop);
	}




	$scope.getNextFiveBusses = function(bustop){
		var routesThatGoByThisStop = [];
		$scope.routes.forEach(function(elem, index, array){
			$http.get('http://www.transit.uga.edu/portal/feed/v3/UGA/stops/'+elem.id)
			//$http.get('http://www.transit.uga.edu/portal/feed/v3/UGA/stops/20')
			.success(function(data, status, headers, config){
				var results = jQuery.grep(data.data, function(e){ return e.id == bustop.id; } );
				if(typeof(results[0]) !== "undefined") routesThatGoByThisStop.push(results[0]);
				//console.log(routesThatGoByThisStop);
			});
		});
		var content = "<strong>Routes that go by this stop:</strong> ";
		routesThatGoByThisStop.forEach(function(elem, index, array){
			console.log(elem);
			$http.get('http://www.transit.uga.edu/portal/feed/v3/UGA/masterRoute/'+elem)
			.success(function(data, status, headers, config){
				console.log(data.data[0].longName);
			});
		});
		$scope.infowindow.setContent("This is a test");
	}

});