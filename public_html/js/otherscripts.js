//GOOGLE ANALYTICS
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-57785436-1', 'auto');
ga('send', 'pageview');



$(document).ready(function(){
	$('#map').height($(window).height());
	$(window).resize(function(){
		$('#map').height($(window).height());
		$('#map').width($(window).width());
	});
	$(".border-menu").click(openclosemenu);
});
$(window).load(function(){

});

var menuopen = false;
function openclosemenu(){
	var time = 500;
	if(menuopen){
		menuopen = false;
		$("#map").animate({marginLeft: "0px"}, time);
		$(".menubutton").animate({left: "0px"}, time, function(){
			$(".menu").css({display: "none"});
		});
	}else{
		$(".menu").css({display: "inline-block"});
		menuopen = true;
		$("#map").animate({marginLeft: "200px"}, time);
		$(".menubutton").animate({left: "200px"}, time);
	}
}
function closemenu(){
	var time = 500;
	if(menuopen){
		menuopen = false;
		$("#map").animate({marginLeft: "0px"}, time);
		$(".menubutton").animate({left: "0px"}, time, function(){
			$(".menu").css({display: "none"});
		});
	}
}
$(".announcement").click(function(){
	$(this).fadeOut();
});
$(window).resize(function(){
	//$(".announcement").height($(".announcement p").height()*2);
});
$(".top-announcement div").click(toggleTopAnnouncement);
function toggleTopAnnouncement(){
	if($(".top-announcement").css("margin-top").replace("px", "") >= 0){
		$(".top-announcement").animate({marginTop: ($(".top-announcement").outerHeight())*-1}, 500);
		$(".top-announcement div span").html("&#9660;");
	}else{
		$(".top-announcement").animate({marginTop: 0}, 500);
		$(".top-announcement div span").html("&#9650;");
	}
}
function zoomToCurrentBusStop(){
	angular.element($('body')).scope().zoomToCurrentBusStop();
}